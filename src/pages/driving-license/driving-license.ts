import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, LoadingController, Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
declare var cordova: any;

/**
 * Generated class for the DrivingLicensePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driving-license',
  templateUrl: 'driving-license.html',
})
export class DrivingLicensePage {
  lastImage: string = null;
  Imgloading: any;
  imgString: string;
  imgLink: string;
  signupObject: any;
  mobilenumber: any;

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private transfer: Transfer,
     public transferObj: TransferObject,
     private file: File,
     public platform : Platform,
     private filePath: FilePath,
     private camera: Camera,
     public actionSheetCtrl: ActionSheetController,
     public toastCtrl: ToastController,
     public loadingCtrl : LoadingController,
     public apiCall :ApiServiceProvider) {



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DrivingLicensePage');
    this.signupObject  = this.navParams.get('signupObj');

    this.mobilenumber = this.signupObject.phoneNum;
    console.log(this.mobilenumber);
  }
  goBack(){
    this.navCtrl.pop();
  }

  // onFileChanged(ev){
  //   console.log(ev);

  // }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }


  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      console.log(this.lastImage);
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  public pathForImage(img) {
    console.log("Image=>",img);
    if (img === null) {
      console.log("If condition");
      return '';
    } else {
      console.log("else condition");
      return cordova.file.dataDirectory + img;
    }
  }



  public uploadImage() {
    // Destination URL
    var url = "https://www.oneqlik.in/users/uploadImage";
    console.log('1');
    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
    console.log("TargetPath=>",targetPath);

    // File name only
    var filename = this.lastImage;
    console.log(filename);
    var options = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      params: { 'fileName': filename }
    };
    // multipart/form-data"
    this.transferObj = this.transfer.create();

    this.Imgloading = this.loadingCtrl.create({ 
      content: 'Uploading...',
    });
    this.Imgloading.present();
    console.log('12');

    // this.apiCall.uploadImage(options).subscribe(res=>{
    //   console.log(res);
    // })
    // Use the FileTransfer to upload the image
    this.transferObj.upload(targetPath, url,options).then(data => {
      console.log("responseUrl=>",data.response);

      // var image = data.response;
      // var splitImage = image.split("/");s
      // var img = splitImage[1];
      // var removeQuote = img.split('"');
      // this.imgString = "/" + removeQuote[0];
      // console.log(this.imgString);
       this.Imgloading.dismissAll();     
       this.dlUpdate(data.response);
     

    }, err => {
      console.log("uploadError=>",err)
      this.lastImage= null;
      this.Imgloading.dismissAll();
      this.presentToast('Error while uploading file, Please try again !!!');
    });
  }


  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  dlUpdate(dllink){
    console.log("mobileNumber=>",this.mobilenumber);
  
    console.log('DlUpdate');
    var dlObj= {
      image_path:dllink,
      phone:this.mobilenumber
    }
    console.log("DLNumber=>",dlObj);
    this.apiCall.updateDL(dlObj)
      .subscribe(res=>{  
        console.log(res);
        this.presentToast('Image succesful uploaded.');   
        this.navCtrl.setRoot(LoginPage);
      },err=>{
        this.presentToast('Internal server Error !!!');   
      })
  
  }

}
