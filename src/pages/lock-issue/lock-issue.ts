import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LockIssuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lock-issue',
  templateUrl: 'lock-issue.html',
})
export class LockIssuePage {
  data: Array<{title: string, details: string, icon: string, showDetails: boolean}> = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // for(let i = 0; i < 2; i++ ){
    //   this.data.push({
    //       title: 'Title '+i,
    //       details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    //       icon: 'ios-add-circle-outline',
    //       showDetails: false
    //     });
    // }

    this.data=[
      {
              title: '1. I am unable to open lock ',
              details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
              icon: 'ios-arrow-forward',
              showDetails: false
            },
            {
              title: '2. Bike with private lock ',
              details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
              icon: 'ios-arrow-forward',
              showDetails: false
            }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LockIssuePage');
  }

  
  toggleDetails(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.icon = 'ios-arrow-forward';
    } else {
        data.showDetails = true;
        data.icon = 'ios-arrow-down';
    }
  }

}
