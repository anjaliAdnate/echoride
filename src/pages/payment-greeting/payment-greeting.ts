import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeedbackPage } from '../feedback/feedback';

/**
 * Generated class for the PaymentGreetingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-greeting',
  templateUrl: 'payment-greeting.html',
})
export class PaymentGreetingPage {
  paymentTime = new Date()
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad PaymentGreetingPage');
    this.paymentTime;
  }

  feedback(){
    this.navCtrl.push(FeedbackPage);
  }

}
