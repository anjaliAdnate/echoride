import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { LockIssuePage } from '../lock-issue/lock-issue';
import { RideBillingPage } from '../ride-billing/ride-billing';
import { ReportMisusePage } from '../report-misuse/report-misuse';
import { BikeIssuePage } from '../bike-issue/bike-issue';

/**
 * Generated class for the ZogomodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-zogomodal',
  templateUrl: 'zogomodal.html',
})
export class ZogomodalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ZogomodalPage');
  }


  dismiss() {
 
    this.viewCtrl.dismiss();
  }

  lockIssues(){
   this.navCtrl.push(LockIssuePage);

  }

  reportMisuse(){
    this.navCtrl.push(ReportMisusePage);
  }
  bikeIssue(){
    this.navCtrl.push(BikeIssuePage);
  }
  rideBilling(){
    this.navCtrl.push(RideBillingPage);
  }
}
