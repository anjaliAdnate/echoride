import { LivePage } from './../live/live';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { BookRidesPage } from '../bookRide/book-ride';
import { ConfirmBookingPage } from '../confirm-booking/confirm-booking';

@IonicPage()
@Component({
  selector: 'page-trip',
  templateUrl: 'trip.html',
})
export class TripPage implements OnInit {

  islogin: any;
  TripData: any;
  deviceId: any;
  distanceBt: number;
  TripStart_Time: string;
  TripEnd_Time: string;
  Durations: string;
  TripAllData: any[];
  tripdata: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,

    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public modalCtrl: ModalController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TripPage');
    this.getTrip();
  }

  ngOnInit() {

  }

  getTrip() {

    this.TripAllData = [];

    console.log("trip Report report");



    var baseURLp = 'https://www.oneqlik.in/trackRouteMap/getTrips?user=' + this.islogin._id + "&purpose=" + 'zogo';
    // this.apicalligi.startLoading().present();
    this.apicalligi.trip_details(baseURLp)
      .subscribe(data => {
        this.TripData = data;
        var fetchTime = this.TripData;
        console.log(fetchTime);
        // this.apicalligi.stopLoading();



        for (var i = 0; i < this.TripData.length; i++) {


          this.TripStart_Time = new Date(this.TripData[i].startSiteEnterAt).toLocaleString();

          this.TripEnd_Time = new Date(this.TripData[i].endSiteExitAt).toLocaleString();

          var fd = new Date(this.TripStart_Time).getTime();
          var td = new Date(this.TripEnd_Time).getTime();
          var time_difference = td - fd;
          var total_min = time_difference / 60000;
          var hours = total_min / 60
          var rhours = Math.floor(hours);
          var minutes = (hours - rhours) * 60;
          var rminutes = Math.round(minutes);
          /* console.log(rhours);
           console.log(rminutes);*/
          this.Durations = rhours + ':' + rminutes
          console.log('Durations', this.Durations);

          this.TripAllData.push({ '_id': this.TripData[i]._id, 'Actual_startTime': this.TripData[i].startsOn, 'Actual_endTime': this.TripData[i].expiresOn, 'Status': this.TripData[i].status, 'Unloading_Site': this.TripData[i].endSite.geoname, 'Loading_Site': this.TripData[i].startSite.geoname, 'durations': this.Durations, 'plannedStartTime': this.TripData[i].startsOn, 'purpose': this.TripData[i].purpose, 'createdOn': this.TripData[i].createdOn });

        }
        console.log('all data', this.TripAllData);
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });

  }

  goto() {
    this.navCtrl.setRoot(BookRidesPage);
  }


  ShowTrip(tripdata) {
    console.log("pppp", tripdata);
    // debugger;
    if (tripdata.Status == "ASSIGNED") {
      var timerTime = tripdata.createdOn;
      localStorage.setItem('timerTIme', JSON.stringify(timerTime));
      localStorage.setItem("tripDetails", JSON.stringify(tripdata));
      this.navCtrl.push(BookRidesPage, {
        tripLocations: tripdata
      });
    } else if ((tripdata.Status == "COMPLETED") || (tripdata.Status == "CANCELLED")) {
      // this.navCtrl.push(LivePage);
    } else if (tripdata.Status == "LOADING") {
      this.navCtrl.push(ConfirmBookingPage);
    } else {
      let toast = this.toast.create({
        message: 'check different Trip !!!',
        duration: 3000,
        position: 'top',
        cssClass: 'toastStyle'
      });
      toast.present();
    }
  }
}
