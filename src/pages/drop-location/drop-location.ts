import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DropLocationSearchPage } from '../drop-location-search/drop-location-search';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GoogleMaps, Marker } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { WalletPage } from '../wallet/wallet';
import { BookRidesPage } from '../bookRide/book-ride';
import { PaytmwalletloginPage } from '../paytmwalletlogin/paytmwalletlogin';
declare var google;

@IonicPage()
@Component({
  selector: 'page-drop-location',
  templateUrl: 'drop-location.html',
})
export class DropLocationPage {
  cartCount: any;
  drop_locationShow: any;
  mapdata: any = {};
  drop_location: any;
  hideButton: boolean;
  navOptions = {
    animation: 'ios-transition'
  };
  dropOffObj: any;
  dstLattitude: any;
  dstLongitude: any;
  locations = [];
  total_km: any;
  Vehicle_count: any;
  dropLocationgeoId: any;
  useridd: any;
  plannedEndTIme: Date;
  deviceId: any;
  pickupgeofenceId: any;
  changeColor: boolean = false;
  polydatageo: any[];
  cordin: any[];
  bounds: any[];
  generalPolygon: any;
  polyRef: any;
  total_distance: string;
  total_veh: any;
  assignVehicle: any;
  showActionSheet: boolean;
  blockedAmt: any;
  constructor(public navCtrl: NavController, private geolocation: Geolocation, public app_api: ApiServiceProvider, public navParams: NavParams, public apiCall: ApiServiceProvider, public toast: ToastController) {
    // debugger;
    var newObj = {}
    newObj = this.apiCall.getGeofence();
    this.total_km = newObj['totalDist'];
    this.Vehicle_count = newObj['totalVeh'];
    this.hideButton = true;
    console.log(newObj);

    var uID = localStorage.getItem('details');

    this.plannedStartTime.setMinutes(this.plannedStartTime.getMinutes() + 10);
    var getData = this.app_api.getGeofence();
    this.pickupgeofenceId = getData['geofenceId'];
    console.log(this.pickupgeofenceId);

    this.useridd = JSON.parse(uID)._id;
    console.log(this.useridd);
    var a = new Date();
    a.setHours(this.plannedStartTime.getHours() + 1, this.plannedStartTime.getMinutes(), this.plannedStartTime.getSeconds());
    this.plannedEndTIme = new Date(a);
    console.log(this.plannedStartTime);
    console.log(this.plannedEndTIme);
    var geofenceOBJ = this.apiCall.getGeofence();
    this.deviceId = geofenceOBJ['device'];
    console.log(this.deviceId);
    this.loadMap();


  }


  ionViewDidLoad() {
    this.getBlockedAmt();
  }

  ionViewDidEnter() {
    console.log("didEnter lifecyclehook")
    this.drop_location = localStorage.getItem("dropOffLocation");
    if (this.drop_location) {
      console.log(JSON.parse(this.drop_location));
      var d = JSON.parse(this.drop_location);
      this.drop_location = JSON.parse(this.drop_location).id;
      this.drop_locationShow = d['name'];
      this.dropOffObj = this.drop_location;
      this.chooseEnd();
    }
  }

  loadMap() {


    this.mapdata.map = GoogleMaps.create('map_canvas1', {
      camera: {
        target: { lat: 20.5937, lng: 78.9629 },
        zoom: 18,
        tilt: 30
      }
    });

    let marker: Marker = this.mapdata.map.addMarkerSync({
      title: 'Current location',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: 43.0741904,
        lng: -89.3809802
      }
    });


    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);



    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  seeNotifications() {

  }


  dropOffLocation() {
    this.navCtrl.push(DropLocationSearchPage, null, this.navOptions)
  }


  chooseEnd() {

    this.app_api.startLoading();
    let that = this;

    var geocoderEnd = new google.maps.Geocoder;
    console.log(this.drop_location)
    geocoderEnd.geocode({ 'placeId': this.drop_location }, (results, status) => {
      if (status === 'OK' && results[0]) {

        that.dstLattitude = results[0].geometry.location.lat();
        that.dstLongitude = results[0].geometry.location.lng();

        var baseURLp = 'https://www.oneqlik.in/zogo/nearby/dst?l=' + this.dstLattitude + ',' + this.dstLongitude;

        that.apiCall.getgeofenceCall(baseURLp)
          .subscribe(data => {
            console.log(data);
            that.app_api.stopLoading();
            that.locations = data.result;
          })
      } else {
        console.log(status);
        that.app_api.stopLoading();
        let toasterr = this.toast.create({
          message: status,
          duration: 3000,
          position: 'top',
          cssClass: 'toastStyle'
        });
        toasterr.present();

      }
    })


  }
  selectedStaion: any;
  autoManufacturers: any;
  plannedStartTime = new Date();


  authanticationBeforeBooking() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.startLoading();
    this.apiCall.paytmValidation(userId)
      .subscribe(res => {
        this.apiCall.stopLoading();
        debugger;
        if ((res.Status == "Token Not Found") || (res.message == "Invalid Token") || (res.status == "FAILURE")) {
          this.navCtrl.push(PaytmwalletloginPage);
        } else if ((res.mobile) && (res.id)) {
          this.getPaytmBalance(userId);
          var paytmNumber = res.mobile;
          localStorage.setItem('paytmregNum', paytmNumber);
        }
      })
  }


  getPaytmBalance(uId) {
    let toast_validation = this.toast.create({
      duration: 3000,
      position: 'middle'
    });

    this.apiCall.startLoading();
    this.apiCall.paytmbalance(uId)
      .subscribe(res => {
        this.apiCall.stopLoading();
        // console.log("response");
        if (res.code == "ETIMEDOUT") {
          console.log("Server Error");
          toast_validation.setMessage("Etimeout server error");
          toast_validation.present();
        } else {
          console.log("response=>", res);
          if (res.response.amount >= this.blockedAmt) {

            this.bookRides();
          } else {
            localStorage.setItem('flag', "pushed before booking ride");
            this.navCtrl.push(WalletPage);
          }
        }
        // if amount is greater than 150 than book ride page after  calling preauth else push user to add money page add balance and come back to previous page and book ride.  
      })
  }



  payRideFare(rideID) {
    this.apiCall.startLoading();
    let toast_validation1 = this.toast.create({
      duration: 3000,
      position: 'middle'
    });
    console.log("rideIdObject=>", rideID)
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    var preAuthObj =
    {
      "CUST_ID": userId,
      "TXN_AMOUNT": this.blockedAmt,
      "DURATIONHRS": "70",
      "app_id": "zogo",
      "ride": rideID._id
    }
    this.apiCall.preAuthantication(preAuthObj)
      .subscribe(res => {
        // console.log("response=>",JSON.parse(res));
        this.apiCall.stopLoading();
        var responseMsg = res;
        var StatusMsg = responseMsg ? responseMsg.STATUSMESSAGE : "";
        var SuccessRes = responseMsg ? responseMsg.STATUS : "";
        var checksumError = responseMsg ? responseMsg.ErrorMsg : '';

        if (SuccessRes == 'TXN_SUCCESS') {
          // this.bookRides();
          this.navCtrl.push(BookRidesPage, null, this.navOptions);

        } else if (checksumError) {
          toast_validation1.setMessage("Internal Server Error");
          toast_validation1.present();
          this.cancelride();
          console.log("checkSumError");
        } else if (StatusMsg) {
          toast_validation1.setMessage(StatusMsg);
          this.cancelride();
          console.log(StatusMsg);
        } else {
          this.cancelride();
          toast_validation1.setMessage("Internal Server Error");
          toast_validation1.present();
        }
      })
  }


  toastMessages(msg) {
    let toastmsgVar = this.toast.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: 'tStyle'
    })


    return toastmsgVar.present();
  }


  bookRides() {
    this.app_api.startLoading();
    let toastmsg = this.toast.create({
      message: 'Please select drop station',
      duration: 3000,
      position: 'top',
      cssClass: 'tStyle'
    })
    if (this.dropStation) {

      var tripDetail = {
        "user": this.useridd,
        "device": this.deviceId,
        "deviceName": "Testingzogo",
        "driver": this.useridd,
        "createdOn": Date.now(),
        "startsOn": this.plannedStartTime,
        "expiresOn": this.plannedEndTIme,
        "tripType": 0,
        "poi": [],
        "purpose": "zogo",
        "startSite": this.pickupgeofenceId,
        "endSite": this.dropLocationgeoId
      }
      this.app_api.savetripDetails(tripDetail)
        .subscribe(res => {
          this.app_api.stopLoading();
          var timerTime = new Date();
          var tempRes = res;
          if (tempRes) {
            this.payRideFare(tempRes);
            localStorage.setItem('timerTIme', JSON.stringify(timerTime));
            localStorage.setItem("tripDetails", JSON.stringify(tempRes));

          } else {

            console.log("cannot Save Trip");
          }
        }, err => {
          if (err.status == 400) {
            this.app_api.stopLoading();
            console.log(err._body);
            // var strhjh = err.body;
            // var tempfoo = JSON.parse(strhjh);
            // console.log(JSON.stringify(tempfoo).message)
            this.toastMessages(err._body);
          }
        })
    } else {
      this.app_api.stopLoading();
      toastmsg.present();
    }

  }

  dropStation: any;
  selectedOption(ev) {
    this.dropStation = ev;
    this.dropLocationgeoId = ev._id
    for (var i = 0; i < this.locations.length; i++) {
      if (this.locations[i]._id != ev._id)
        this.locations[i].changeColor = false;
      else
        this.locations[i].changeColor = true;
    }
    localStorage.setItem("droplocatoingeoId", JSON.stringify(ev));
  }

  goBack() {
    this.navCtrl.pop();
  }


  getBlockedAmt() {
    this.app_api.startLoading();
    this.app_api.getblockamt()
      .subscribe(res => {
        this.app_api.stopLoading();
        console.log("billing amount=>", res['Block Amount']);
        this.blockedAmt = res['Block Amount'];

      }, err => {
        this.app_api.stopLoading();
        console.log(err);
      })
  }

  cancelride() {
    var reason = "plan changed"
    var cancellationTime = new Date();
    var tempDetail = localStorage.getItem('tripDetails');
    console.log("kkk", tempDetail)
    var tempRouteDetail = JSON.parse(tempDetail);
    console.log(tempRouteDetail);
    console.log("identifier=>", tempDetail)
    var cancelObj = {
      "_id": tempRouteDetail._id,
      "cancelledAt": cancellationTime,
      "status": "CANCELLED",
      "device": tempRouteDetail.device,
      "cancelledBy": this.useridd,
      "cancellationReason": reason

    };
    console.log("cancelk", cancelObj);
    this.app_api.startLoading();
    this.app_api.canceltripDetails(cancelObj)
      .subscribe(res => {
        this.app_api.stopLoading();
        console.log("inside alert success");

        console.log("getResponse", res);
        localStorage.removeItem('tripDetails');
        localStorage.removeItem('pickupLocation');
        localStorage.removeItem("dropOffLocation");
        localStorage.removeItem('timerTIme');
        localStorage.removeItem('lockRide');

      }, err => {
        this.app_api.stopLoading();
        console.log('error in cancel ride=> ', err);
      })
    console.log('Ok clicked');
  }
}
