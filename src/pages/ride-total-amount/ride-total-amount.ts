import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { PaymentSecurePage } from '../payment-secure/payment-secure';
import { WalletPage } from '../wallet/wallet';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { PaytmwalletloginPage } from '../paytmwalletlogin/paytmwalletlogin';

@IonicPage()
@Component({
  selector: 'page-ride-total-amount',
  templateUrl: 'ride-total-amount.html',
})
export class RideTotalAmountPage {
  rideTime: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public apiCall: ApiServiceProvider,private toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RideTotalAmountPage');
    this.rideTime = 12;
    // JSON.parse(localStorage.getItem('totalrideTime'));
    console.log(this.rideTime);
    this.rideFare(this.rideTime);

  }

  makePayment(){
    var useDetails =  JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;

    //  var userId = "5c1a28bc4e095c5648b3e6d0"
    console.log(userId);
    this.apiCall.paytmValidation(userId)
    .subscribe(res=>{
      console.log(res);
      if((res.Status == "Token Not Found")||(res.message ==  "Invalid Token")||(res.status ==  "FAILURE")){
            this.navCtrl.push(PaytmwalletloginPage);
      }else if((res.mobile)&&(res.id)){
        this.getPaytmBalance(userId);
        var paytmNumber = res.mobile;
        localStorage.setItem('paytmregNum',paytmNumber);
      }
    })
  }

  getPaytmBalance(uId){  
    this.apiCall.paytmbalance(uId)
      .subscribe(res=>{
        console.log("response");
        if(res.code== "ETIMEDOUT"){
          this.toasterror(res.code)
          console.log("Server Error");
        }else{
          localStorage.setItem('paytmBalance',JSON.stringify(res));
          this.navCtrl.push(WalletPage);
        }
     
      })
  }
  toasterror(errstatus){
    let toastCtrl  = this.toastCtrl.create({
      duration: 3000,
      position: 'top',
      cssClass : 'toastStyle'
    })
    if(errstatus == "ETIMEDOUT"){
      toastCtrl['message'] = "Internal Server error";
      toastCtrl.present()

    }

  }
  amtToBePaid:any;
  rideFare(fare){
    this.apiCall.rideFare(fare)
        .subscribe(res=>{
          console.log(res.billAmount);
          this.amtToBePaid = res.billAmount;
          localStorage.setItem("tripFare",JSON.stringify(this.amtToBePaid));
        })
  }

}
