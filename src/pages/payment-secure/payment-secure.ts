import { LivePage } from './../live/live';
import { WalletPage } from './../wallet/wallet';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { PaymentGreetingPage } from '../payment-greeting/payment-greeting';
import { PaytmwalletloginPage } from '../paytmwalletlogin/paytmwalletlogin';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { NetworkInterface } from '@ionic-native/network-interface';

/**
 * Generated class for the PaymentSecurePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-secure',
  templateUrl: 'payment-secure.html',
})
export class PaymentSecurePage {
  paytmregNum: string;
  rideFare: any;
  amounttobepaid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public apiCall: ApiServiceProvider,private toastCtrl:ToastController, private networkInterface: NetworkInterface) {
   this.rideFare =  this.navParams.get('rideFare');
   console.log(this.rideFare);
  }

  ionViewDidLoad() { 
    this.amounttobepaid  = JSON.parse(localStorage.getItem('tripFare'));
    console.log(this.amounttobepaid);

    this.getNetworkDetal();
  }

  proceedSecurely(){

    // PaytmwalletloginPage

   this.paytmregNum = localStorage.getItem('paytmregNum');
   var  useDetails =  JSON.parse(localStorage.getItem('details'));
   var  userId = useDetails._id;
    console.log(userId);
    let toastCtrl  = this.toastCtrl.create({
      message : "Trip amount paid, Thanks for using zogo ride !!",
      duration: 3000,
      position: 'top',
      cssClass : 'toastStyle'
    })
    let toastCtrlfailure  = this.toastCtrl.create({
      message : "Payment failed , Try again",
      duration: 3000,
      position: 'top',
      cssClass : 'toastStyle'
    })

    let toastCtrlServerError  = this.toastCtrl.create({
      message : "Internal Server Error, Try Again",
      duration: 3000,
      position: 'top',
      cssClass : 'toastStyle'
    })


    let updatePaymentStatus = this.toastCtrl.create({
      message : "Unable to update Payment Status ",
      duration: 3000,
      position: 'top',
      cssClass : 'toastStyle'
    })



    var paymentWithdraw = {
      CUST_ID : userId,
      app_ip : "192.168.1.102",
      deviceId : this.paytmregNum,
      "app_id" :"zogo",
      "TXN_AMOUNT":this.amounttobepaid
    }
    this.apiCall.startLoading();
    this.apiCall.proceedSecurely(paymentWithdraw)
    .subscribe(res=>{
      this.apiCall.stopLoading();
      console.log(res);
      if(res.Status == "TXN_SUCCESS"){
        this.apiCall.updateTripStatus(userId).subscribe(res=>{
          console.log("updateStatusResponse=>",res);
          if(res.payment_status == true){
            toastCtrl.present()
            toastCtrl.onDidDismiss(() => {
              localStorage.removeItem('flag');
              this.navCtrl.setRoot(LivePage);
          })

        }else{

          updatePaymentStatus.present();

        }
        
        })
       
      }else if(res.STATUS == "TXN_FAILURE"){
        console.log("Transaction failed ");
        toastCtrlfailure.present();
      }else{
        toastCtrlServerError.present();
      }      
    })
  }

  getNetworkDetal(){
    this.networkInterface.getCarrierIPAddress()
    .then(address => console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`))
    .catch(error => console.error(`Unable to get IP: ${error}`));

  }
  toasterror(errstatus){
    console.log(errstatus);
    let toastCtrl1  = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass : 'toastStyle'
    })
  
      toastCtrl1['message'] = errstatus;
      console.log(toastCtrl1);
      return toastCtrl1.present();
    

  }

  releaseAMT(){
    var  useDetails =  JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;

 this.apiCall.startLoading();
 var releaseObj = {
  CUST_ID:userId,
  app_id : 'zogo'
  }
  this.apiCall.releaseAmount(releaseObj)
    .subscribe(res=>{
      if(res.STATUS == "TXN_SUCCESS"){
        this.apiCall.stopLoading(); 
        this.proceedSecurely();
      }else{
        console.log("Please Try again");
       this.toasterror("try aghain");
      }
    
    },err=>{
      console.log("Internal server Error")
      this.apiCall.stopLoading();
      this.toasterror("try aghain");
    })
}
}
