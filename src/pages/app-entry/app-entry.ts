import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-app-entry',
  templateUrl: 'app-entry.html',
})
export class AppEntryPage {
  splash = true;
  useridd: any;

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppEntryPage');
  }

  signUpPage() {
    // debugger
    this.navCtrl.push(SignupPage)
  }

  loginPage() {
    this.navCtrl.push(LoginPage)
  }
}
