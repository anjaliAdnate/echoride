import { LoginPage } from './../login/login';
import { Component } from "@angular/core";
import { NavController, NavParams, ToastController, AlertController, ViewController, Platform } from "ionic-angular";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { DrivingLicensePage } from '../driving-license/driving-license';
declare var SMS :any;
declare var cordova: any;

@Component({
    selector: 'page-signup',
    templateUrl: './signupOtp.html'
})
export class SignupOtp {
    signupForm: FormGroup;
    signupverify: any;
    mobileNum: string;
    fetchOtp: any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public androidPermissions: AndroidPermissions,
        public formBuilder: FormBuilder,
        public apiService: ApiServiceProvider,
        public platform: Platform, 
        private toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public viewCtrl: ViewController) {
        this.mobileNum = localStorage.getItem('mobnum');
        this.signupForm = formBuilder.group({
            otp1: ["", Validators.required],
            otp2: ["", Validators.required],
            otp3: ["", Validators.required],
            otp4: ["", Validators.required]

        })

    }

    // ionViewWillEnter() {
    //     console.log('will enter starts...');
    //     debugger;
    //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(

    //         success => console.log('Permission granted'),
    //         err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    //     );

    //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
    //     this.Otp_login();
    // }



    signupUser() {
        
        console.log(this.signupForm.value.otp1);
        var otp = this.signupForm.value.otp1 + this.signupForm.value.otp2 + this.signupForm.value.otp3 + this.signupForm.value.otp4
        /* console.log("LOGIN user: " + $scope.usersignup.firstname + " - PW: " + usersignup.password);*/
        console.log(otp);
        var usersignup = {
            "phone": this.mobileNum, 
            "otp": otp
        }


        this.apiService.startLoading();
        this.apiService.siginupverifyCall(usersignup)
            .subscribe(data => {
                console.log(data);
                this.apiService.stopLoading();
                this.signupverify = data;
                console.log(this.signupverify);
                this.signupverify.phoneNum = this.mobileNum;
                let toast = this.toastCtrl.create({
                    message: 'Mobile verify  successfully',
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    console.log('Dismissed toast');
                    this.navCtrl.push(DrivingLicensePage,{signupObj:this.signupverify});
                });

                toast.present();
            },
                err => {
                    let toastErr = this.toastCtrl.create({
                        message: 'Internal Server error',
                        position: 'bottom',
                        duration: 2000
                    });
                    toastErr.present();
                    this.apiService.stopLoading();
                    var body = err._body;
                    var msg = JSON.parse(body);
                    let alert = this.alerCtrl.create({
                        title: 'Oops!',
                        message: msg.message,
                        buttons: ['OK']
                    });
                    alert.present();
                })
    }



    resendOtp(){
        
    }

    // Otp_login() {
    //     debugger;
    //     this.platform.ready().then((readySource) => {
    //         console.log("we are here ")
    //         debugger;
    //         if (SMS) SMS.startWatch(() => {
    //             console.log('watching started');
    //         }, Error => {
    //             console.log('failed to start watching');
    //         });

    //         document.addEventListener('onSMSArrive', (e: any) => {
    //             debugger;
    //             var sms = e.data;
    //             console.log("autoReadSMS",sms);
    //             this.fetchOtp = e.data.body.match(/[0-9]+/g);
    //             console.log(this.fetchOtp);
    //         });
    //     })
    // }

    goBack(){
        this.navCtrl.pop();
    }

}