import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@IonicPage()
@Component({
  selector: 'page-bike-issue',
  templateUrl: 'bike-issue.html',
})
export class BikeIssuePage {
  BikeIssueForm: FormGroup;
  scannedCode: string;
  address_show: any;
  locationShow: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public barcodeScanner: BarcodeScanner,public formBuilder: FormBuilder, public geolocation: Geolocation) {

    this.BikeIssueForm = formBuilder.group({
      name: ['', Validators.required],
      bikenumber: ['', Validators.required],
      // mobile: ['', Validators.required],
      // title: ['', Validators.required],
      note: ['', Validators.required],
      location:  ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BikeIssuePage');
  }


  ScanQrcode() {
    var that = this;
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
      console.log('scan qrcode: ', this.scannedCode);
      
    }, (err) => {
      console.log('Error: ', err);

    });
  }


  BikeLocation(){
    console.log('location');
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(' getting location', resp);

      var that = this;
     
    
      var initLat =resp.coords.latitude;
      var initLng = resp.coords.longitude;
      console.log(initLat,initLng);
   
          var geocoder = new google.maps.Geocoder();
          var latlng = new google.maps.LatLng(initLat, initLng);
      
          var request = {
            latLng: latlng
          };
  
          geocoder.geocode(request, function (data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (data[0] != null) {
                that.address_show = data[0].formatted_address;
                // device.locationAddress = outerThis.address_show;
                that.locationShow = that.address_show;
                
                console.log(that.locationShow);
               
              } else {
      
                that.address_show = data[0].formatted_address;
              
                console.log("address=>",that.address_show);
              }
            }
            else {
          
            }
      
          })
         
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
    
  }

  
}
