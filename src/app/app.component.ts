import { MyProfilePage } from './../pages/my-profile/my-profile';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, ModalController, AlertController, App, ToastController, Keyboard } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { NetworkProvider } from '../providers/network/network';
import { MenuProvider } from '../providers/menu/menu';
import { SideMenuOption } from '../../shared/side-menu-content/models/side-menu-option';
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { SideMenuSettings } from '../../shared/side-menu-content/models/side-menu-settings';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { LivePage } from '../pages/live/live';
import { AppEntryPage } from '../pages/app-entry/app-entry';
import { ConfirmBookingPage } from '../pages/confirm-booking/confirm-booking';
import { BookRidesPage } from '../pages/bookRide/book-ride';
import { RideTotalAmountPage } from '../pages/ride-total-amount/ride-total-amount';
import { ApiServiceProvider } from '../providers/api-service/api-service';

@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // Get the instance to call the public methods
  @ViewChild(SideMenuContentComponent) sideMenu: SideMenuContentComponent;

  rootPage: any;

  // pages: Array<{ title: string, component: any, icon?: string }>;
  pages: any;
  selectedMenu: any;

  islogin: any;
  setsmsforotp: string;
  DealerDetails: any;
  dealerStatus: any;
  // public cartCount: number = 0;

  // Options to show in the SideMenuContentComponent
  public options: Array<SideMenuOption>;

  // Settings for the SideMenuContentComponent
  public sideMenuSettings: SideMenuSettings = {
    accordionMode: true,
    showSelectedOption: true,
    selectedOptionClass: 'active-side-menu-option'
  };
  private unreadCountObservable: any = new ReplaySubject<number>(0);
  userdetails: any;
  tripStatus: any;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    public networkProvider: NetworkProvider,
    public menuProvider: MenuProvider,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    // private push: Push,
    public alertCtrl: AlertController,
    public app: App,
    public appAPi: ApiServiceProvider,
    public toast: ToastController,
    public keyboard: Keyboard
  ) {
    var s = 0;

    this.events.subscribe('user:updated', (udata) => {
      this.islogin = udata;
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();

      this.checkRideStatus();
    });
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.setsmsforotp = localStorage.getItem('setsms');

    this.getSideMenuData();
    this.initializeApp();
  }

  getSideMenuData() {
    this.pages = this.menuProvider.getSideMenus();
  }

  backBtnHandler() {
    this.platform.registerBackButtonAction(() => {

      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();

      if (activeView.name === "LivePage") {

        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else {
          const alert = this.alertCtrl.create({
            title: 'App termination',
            message: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        }
      }
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {

      // that.pushNotify();

      that.networkProvider.initializeNetworkEvents();

      // Offline event
      that.events.subscribe('network:offline', () => {
        alert('network:offline ==> ' + this.networkProvider.getNetworkType());
      });

      // Online event
      that.events.subscribe('network:online', () => {
        alert('network:online ==> ' + this.networkProvider.getNetworkType());
      });

      that.backBtnHandler();
    });

    // Initialize some options
    that.initializeOptions();
    // Change the value for the batch every 5 seconds
    setInterval(() => {
      this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
    }, 5000);
  }


  checkRideStatus() {
    
    var uID = this.islogin._id;
  
    if (uID) {
      this.appAPi.checkRideStatus(uID)
        .subscribe(res => {
          var currentStatus = res.status;
          if (currentStatus == "COMPLETED") {
            if (res.payment_status == false) {
              this.rootPage = RideTotalAmountPage;
            }
            else if (res.payment_status == true) {
              this.rootPage = LivePage;
              // LivePage
            }
          }
          else if (currentStatus == "ASSIGNED") {
            this.rootPage = BookRidesPage;
            // BookRidesPage;
          } else if (currentStatus == "LOADING") {
            this.rootPage = ConfirmBookingPage;
            //  ConfirmBookingPage ; 
          }
          else if (localStorage.getItem("loginflag") == "loginflag") {
            this.rootPage = LivePage;
            // LivePage
          } else {
            this.rootPage = AppEntryPage;
            // AppEntryPage
          }
        }, err => {
          console.log("error occured");
          if (localStorage.getItem("loginflag") == "loginflag") {
            this.rootPage = LivePage;

            // LivePage
          } else {
            this.rootPage = AppEntryPage;
            // AppEntryPage
          }
        })
    } else {
      this.rootPage = AppEntryPage;
    }
  }


  private initializeOptions(): void {
    this.options = new Array<SideMenuOption>();

    // Load simple menu options analytics
    // ------------------------------------------
    this.options.push({
      iconName: 'map',
      displayText: 'Home',
      component: 'LivePage'
    });


    this.options.push({
      iconName: 'map',
      displayText: 'My Trip',
      component: 'TripPage'
    });
  }


  public onOptionSelected(option: SideMenuOption): void {
    this.menuCtrl.close().then(() => {
      if (option.custom && option.custom.isLogin) {
        this.presentAlert('You\'ve clicked the login option!');
      } else if (option.custom && option.custom.isLogout) {
        this.presentAlert('You\'ve clicked the logout option!');
      } else if (option.custom && option.custom.isExternalLink) {
        let url = option.custom.externalUrl;
        window.open(url, '_blank');
      } else {
        // Get the params if any
        const params = option.custom && option.custom.param;

        // Redirect to the selected page
        this.nav.setRoot(option.component, params);
      }
    });
  }

  public collapseMenuOptions(): void {
    this.sideMenu.collapseAllOptions();
  }

  public presentAlert(message: string): void {
    let alert = this.alertCtrl.create({
      title: 'Information',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  openPage(page, index) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component) {
      this.nav.setRoot(page.component);
      this.menuCtrl.close();
    } else {
      if (this.selectedMenu) {
        this.selectedMenu = 0;
      } else {
        this.selectedMenu = index;
      }
    }
  }

  logout() {
    localStorage.clear();
    localStorage.setItem('count', null)
    this.menuCtrl.close();
    this.nav.setRoot(LoginPage);
  }

  gotoUserPage() {
    this.nav.push(MyProfilePage);
  }

}
