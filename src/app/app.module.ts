import { TransferObject, Transfer } from '@ionic-native/transfer';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPageModule } from '../pages/login/login.module';
import { BookRidesModule } from '../pages/bookRide/book-ride.module';
import { DriversRepPageModule } from '../pages/drivers-rep/drivers-rep.module';
import { LivePageModule } from '../pages/live/live.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { NetworkProvider } from '../providers/network/network';
import { MobileVerify } from '../pages/login/mobile-verify';
import { SignupOtp } from '../pages/signup/signupOtp';
import { NetworkInterface } from '@ionic-native/network-interface';
import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';

import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { TripPageModule } from '../pages/trip/trip.module';

import { WalletPageModule } from '../pages/wallet/wallet.module';
import { MyProfilePageModule } from '../pages/my-profile/my-profile.module';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { RideDetailsPageModule } from '../pages/ride-Details/rideDetails.module';

import { ZogomodalPageModule } from '../pages/zogomodal/zogomodal.module';
import { ReferEarnPageModule } from '../pages/refer-earn/refer-earn.module';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { ComponentsModule } from '../components/components.module';
import { BookingDetailComponent } from '../components/booking-detail/booking-detail';
import { AppEntryPageModule } from '../pages/app-entry/app-entry.module';
// import { AppEntryPage } from '../pages/app-entry/app-entry';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { SMS } from '@ionic-native/sms';
import { PaytmwalletloginPageModule } from '../pages/paytmwalletlogin/paytmwalletlogin.module';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { RideTotalAmountPageModule } from '../pages/ride-total-amount/ride-total-amount.module';
import { ConfirmBookingPageModule } from '../pages/confirm-booking/confirm-booking.module';
import { DropLocationPageModule } from '../pages/drop-location/drop-location.module';
import { BikeIssuePageModule } from '../pages/bike-issue/bike-issue.module';
import { DrivingLicensePageModule } from '../pages/driving-license/driving-license.module';
import { DropLocationSearchPageModule } from '../pages/drop-location-search/drop-location-search.module';
import { PickupLocationSearchPageModule } from '../pages/pickup-location-search/pickup-location-search.module';
import { PaymentGreetingPageModule } from '../pages/payment-greeting/payment-greeting.module';
import { FeedbackPageModule } from '../pages/feedback/feedback.module';
import { PaymentSecurePageModule } from '../pages/payment-secure/payment-secure.module';
import { SignupPage2PageModule } from '../pages/signup-page2/signup-page2.module';
import { LockIssuePageModule } from '../pages/lock-issue/lock-issue.module';
import { ReportMisusePageModule } from '../pages/report-misuse/report-misuse.module';
import { RideBillingPageModule } from '../pages/ride-billing/ride-billing.module';
import { MenuProvider } from '../providers/menu/menu';

@NgModule({
  declarations: [
    MyApp,
    SideMenuContentComponent,
    MobileVerify,
    SignupOtp
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: true,
      scrollAssist: true,
      autoFocusAssist: true
    }),
    IonBottomDrawerModule,
    BookRidesModule,
    BikeIssuePageModule,
    RideTotalAmountPageModule,
    ConfirmBookingPageModule,
    DropLocationPageModule,
    DriversRepPageModule,
    LivePageModule,
    LoginPageModule,
    SignupPageModule,
    SelectSearchableModule,
    AppEntryPageModule,
    TripPageModule,
    MyProfilePageModule,
    ComponentsModule,
    PaytmwalletloginPageModule,
    RideDetailsPageModule,
    ZogomodalPageModule,
    ReferEarnPageModule,
    WalletPageModule,
    DrivingLicensePageModule,
    DropLocationSearchPageModule,
    PickupLocationSearchPageModule,
    PaymentGreetingPageModule,
    FeedbackPageModule,
    PaymentSecurePageModule,
    LockIssuePageModule,
    ReportMisusePageModule,
    RideBillingPageModule,
    SignupPage2PageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MobileVerify,
    SignupOtp,
    BookingDetailComponent,
    // AppEntryPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServiceProvider,
    Network,
    NetworkProvider,
    NetworkInterface,
    GoogleMaps,
    Spherical,
    Geolocation,
    BarcodeScanner,
    LocationAccuracy,
    SocialSharing,
    InAppBrowser,
    FileTransfer,
    FileTransferObject,
    SMS,
    Transfer,
    TransferObject,
    File,
    Camera,
    FilePath,
    AndroidPermissions,
    SMS,
    MenuProvider
  ]
})
export class AppModule { }
