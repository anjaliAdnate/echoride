import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import { LoadingController } from 'ionic-angular';

@Injectable()
export class ApiServiceProvider {
  private headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
  link: string = "https://www.oneqlik.in/users/";
  loading: any;
  appId="zogo";
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController) {
    console.log('Hello ApiServiceProvider Provider');
  }
  // 13.126.36.205
  ////////////////// LOADING SERVICE /////////////////
  startLoading() {
    console.log("loading Start");
    this.loading= this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
    return this.loading.present();
  }

  stopLoading() {
    return this.loading.dismiss();
  }
  ////////////////// END LOADING SERVICE /////////////

  getDriverList(_id) {
    return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid="+_id, {headers: this.headers})
    .map(res => res.json());
  }

  filterByDateCall(_id, skip: Number, limit: Number, dates) {
    // console.log("from date => "+ dates.fromDate.toISOString())
    // console.log("new date "+ new Date(dates.fromDate).toISOString())
    var from = new Date(dates.fromDate).toISOString();
    var to = new Date(dates.toDate).toISOString();
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  filterByType(_id, skip: Number, limit: Number, key) {
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&type=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getFilteredcall(_id, skip: Number, limit: Number, key) {
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getDataOnScroll(_id, skip: Number, limit: Number) {
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleListCall(_id, email) {
    return this.http.get('https://www.oneqlik.in/devices/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
      .map(res => res.json());
  }

  trip_detailCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  trip_details(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteDataCall(data) {
    return this.http.post("https://www.oneqlik.in/trackRoute", data, { headers: this.headers })
      .map(res => res.json());
  }

  savetripDetails(data) {
    return this.http.post("https://www.oneqlik.in/trackRouteMap", data)
      .map(res => res.json());
  }

  canceltripDetails(a){
    
    return this.http.post("https://www.oneqlik.in/trackRouteMap/cancelRide", a)
    .map(res => res);
  }

  updatetripDetails(a){
    console.log("update trip")
    return this.http.post("https://www.oneqlik.in/trackRouteMap/updateRide", a)
    .map(res => res);
  }

  completetripDetails(a){
    return this.http.post("https://www.oneqlik.in/trackRouteMap/rideComplete", a)
    .map(res => res);
  }

  updateRoute(edata){
    return this.http.post("https://www.oneqlik.in/trackRouteMap/updateRoute_map", edata)
    .map(res => res.json());
  }

  gettrackRouteCall(link, data) {
    return this.http.post(link, data, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getRoutesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getStoppageApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("https://www.oneqlik.in/stoppage/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getIgiApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("https://www.oneqlik.in/notifs/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getOverSpeedApi(starttime, endtime, overSpeeddevice_id, _id) {
    return this.http.get("https://www.oneqlik.in/notifs/overSpeedReport?from_date=" + starttime + '&to_date=' + endtime + '&device=' + overSpeeddevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getGeogenceReportApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("https://www.adnateiot.com/notifs/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getFuelApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get("https://www.oneqlik.in/notifs/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceReportApi(starttime, endtime, _id, Ignitiondevice_id) {
    return this.http.get("https://www.oneqlik.in/summary/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
      .map(res => res.json());
  }

  getDailyReport(email, from, to) {
    return this.http.get("https://www.adnateiot.com/gps/getGpsReport?email=" + email + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  contactusApi(contactdata) {
    return this.http.post(this.link + "contactous", contactdata, { headers: this.headers })
      .map(res => res.json());
  }

  getAllNotificationCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addgeofenceCall(data) {
    return this.http.post('https://www.oneqlik.in/geofencing/addgeofence', data, { headers: this.headers })
      .map(res => res);
  }

  getdevicegeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofencestatusCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGeoCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getallgeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  
  getgeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  user_statusCall(data) {
    return this.http.post('https://www.oneqlik.in/users/user_status', data, { headers: this.headers })
      .map(res => res);
  }

  editUserDetailsCall(devicedetails) {
    return this.http.post('https://www.adnateIot.com/users/editUserDetails', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getAllDealerVehiclesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addGroupCall(devicedetails) {
    return this.http.post('https://www.oneqlik.in/groups/addGroup', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleTypesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getAllUsersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getDeviceModelCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  groupsCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addDeviceCall(devicedetails) {
    return this.http.post('https://www.oneqlik.in/devices/addDevice', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getCustomersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofenceCall(_id) {
    return this.http.get("https://www.oneqlik.in/geofencing/getgeofence?uid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassApi(mobno) {
    return this.http.get(this.link + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassMobApi(Passwordset) {
    return this.http.get(this.link + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
      .map(res => res.json());
  }

  loginApi(userdata) {
    return this.http.post(this.link + "LoginWithOtp", userdata, { headers: this.headers })
      .map(res => res.json());
  }

  signupApi(usersignupdata) {
    return this.http.post('https://www.oneqlik.in/users' + "/signUpZogo", usersignupdata, { headers: this.headers })
      .map(res => res.json());
  }

  updateDL(updateDL){
    return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL)
    .map(res => res.json());
  }

  resendOtp(phnum){
    return this.http.post('https://www.oneqlik.in/users' + "/sendOtpZRides", phnum)
      .map(res => res.json());
  }

  dashboardcall(email, from, to, _id) {
    return this.http.get('https://www.oneqlik.in/gps/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  stoppedDevices(_id, email, off_ids) {
    return this.http.get('https://www.oneqlik.in/devices/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
      .map(res => res.json());
  }

  livedatacall(_id, email) {
    return this.http.get("https://www.oneqlik.in/devices/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
      .map(res => res.json());
  }

  getdevicesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  ignitionoffCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deviceupdateCall(devicedetail) {
    return this.http.post("https://www.oneqlik.in/devices/deviceupdate", devicedetail, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceSpeedCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  stoppage_detail(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  gpsCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getSummaryReportApi(starttime, endtime, _id, device_id) {
    return this.http.get("https://www.oneqlik.in/summary?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  getallrouteCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getSpeedReport(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deviceupdateInCall(devicedetail) {
    return this.http.post("https://www.adnateIot.com/devices/deviceupdate", devicedetail, { headers: this.headers })
      .map(res => res.json());
  }

  deleteDeviceCall(d_id) {
    return this.http.get("https://www.oneqlik.in/devices/deleteDevice?did=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }

  deviceShareCall(data) {
    return this.http.get("https://www.adnateiot.com/devices/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
      .map(res => res.json());
  }

  pushnotifyCall(pushdata) {
    return this.http.post("https://www.adnateiot.com/users/PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }


  getGroupCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGroupCall(d_id) {
    return this.http.get("https://www.oneqlik.in/groups/deleteGroup?_id=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }



  addcustomerCall(devicedetails) {
    return this.http.post('https://www.oneqlik.in/users/signUp', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }


  getAllDealerCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }


  siginupverifyCall(usersignup){
    return this.http.post('https://www.oneqlik.in/users' + "/signUpZogo", usersignup, { headers: this.headers })
    .map(res => res.json());
  }



  pickupGeofence={};
 
  setGeofenceData(pickGeoObj){
    this.pickupGeofence = pickGeoObj;
    console.log(this.pickupGeofence);
  }

  getGeofence(){
    console.log("getFunc")
    return this.pickupGeofence;
  }

  // paytm integration

  
// https://www.oneqlik.in/paytm/sendOTP

paytmValidation(userCred){
  return this.http.get('https://www.oneqlik.in/paytm/ValidateToken?CUST_ID='+ userCred + "&app_id=" +this.appId)
    .map(res=> res.json());
}

paytmbalance(userCred){
  return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID='+ userCred  + "&app_id=" +this.appId)
    .map(res=> res.json());
}

addMoneyPaytm(amtObj){
  return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj)
  .map(res=> res);
}

  paytmLoginWallet(walletcredentials){
    return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials)
    .map(res=> res.json());
  }
  paytmOTPvalidation(otpObj){
    return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj)
    .map(res=> res.json());
  }

  paytmAddMount(amountObj){ 
    return this.http.post('https://pguat.paytm.com/oltp-web/processTransaction', amountObj)
    .map(res=> res);
  }

  preAuthantication(preAuthObj){ 
    return this.http.post('https://www.oneqlik.in/paytm/preAuth', preAuthObj)
    .map(res=> res.json());
  }

  proceedSecurely(withdrawObj){
    return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj)
    .map(res=> res.json());
  }

  getblockamt(){
 //   http://localhost:3000/zogo/getInnitialBlockAmount
    return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
    .map(res=> res.json());
  }


  releaseAmount(uid){
    return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid)
    .map(res=> res.json());
  
  }
  rideFare(fare){
    return this.http.get('https://www.oneqlik.in/zogo/getBillingAmount?time='+ fare)
    .map(res=> res.json());
  }

  checkTripStatus(id){
    console.log(id);
    return this.http.get('https://www.oneqlik.in/trackRouteMap/getRideStatus?_id='+ id)
    .map(res=> res.json());
  }

  pauseRide(payloadPause){
    console.log(payloadPause);
    return this.http.post('https://www.oneqlik.in/trackRouteMap/zogoPause', payloadPause)
    .map(res=> res.json());
  }

  resumeRide(payloadRestart){
    console.log(payloadRestart);
    return this.http.post('https://www.oneqlik.in/trackRouteMap/zogoRestart', payloadRestart)
    .map(res=> res.json());
  }


  checkRideStatus(uId){
    return this.http.get('https://www.oneqlik.in/zogo/getLastRideStatusByUser?user='+ uId)
    .map(res=> res.json());

  }

  addcommandQueue(devImei){
    return this.http.post("https://www.oneqlik.in/trackRouteMap/addCommandQueue", devImei)
    .map(res => res.json());
  }

  updateTripStatus(uId){
   
    return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId)
    .map(res=> res.json());
  }
  
 
  navigateTO() {
    return this.http.get('https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393&query_place_id=ChIJKxjxuaNqkFQR3CK6O1HNNqY')
      .map(res => res.json());
  }

}


